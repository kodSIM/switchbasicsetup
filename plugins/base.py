"""Module for manage switch by COM-port.
Use Cisco IOS syntax for command line interface"""

import serial
from time import sleep
import re
from abc import *

class BaseSwitch:
    """Basic class for switch management."""
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, port='COM1'):
        """Constructor for Switch"""
        self._prompt_nonpriv = re.compile(r'>$')
        self._prompt_priv_conf = re.compile(r'#$')
        self._prompt_more = re.compile(r'--More--')
        self._find_interfaces = re.compile(r'(((Gi)|(Te)|(Fa))+([0-9]|\/)+)')
        self._console = serial.Serial(port=port, timeout=8)
        self._command = {
            'priv': 'enable',
            'exit': 'exit',
            'interfaces': 'show interface status'
        }

    def check_nonpriv(self, output):
        """Check nonprivilege mode"""
        if self._prompt_nonpriv.search(output):
            return True
        else:
            return False
        
    def check_priv_conf(self, output):
        """Check privilege or configure mode"""
        if self._prompt_priv_conf.search(output):
            return True
        else:
            return False

    def check_more(self, output):
        """Check multipage output"""
        if self._prompt_more.search(output):
            return True
        else:
            return False

    def send_command(self, command):
        """Send command to switch"""
        self._console.write('\n')
        sleep(1)
        self._console.write(command + '\n')
        result = ''
        while True:
            sleep(1)
            res = self._console.read(self._console.in_waiting)
            if self.check_priv_conf(res):
                result = result + res
                break
            elif self.check_more(res):
                result = result + res
                self._console.write(' ')
                #sleep(1)
                continue
            else:
                result = result + res
        return result
    
    def get_interfaces(self):
        """Get list interfaces from switch"""
        ifaces = self.send_command(self._command['interfaces'])
        temp = self._find_interfaces.findall(ifaces)
        ifaces = []
        for i in temp:
            ifaces.append(i[0])
        return ifaces
    
    def login(self):
        """Login console"""
        self._console.write('\r\n')
        sleep(1)
        output = self._console.read(self._console.in_waiting)
        if self.check_nonpriv(output):
            output = self.send_command(self._command['priv'])
        if self.check_priv_conf(output):
            print("Login is done")
            return True
        else:
            print("Login is failed")
            return False

    def logout(self):
        """Logout console"""
        self._console.close()
        print("Logout is done")

if __name__ == "__main__":
    print("This is library module")