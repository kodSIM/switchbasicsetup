"""Module for manage switch by COM-port.
Use Cisco IOS syntax for command line interface"""

import re
import serial

import base

class IOS(base.BaseSwitch):
    """Class for cisco switch management"""

    def __init__(self, port='COM1'):
        """Constructor for Switch"""
        self._prompt_nonpriv = re.compile(r'>$')
        self._prompt_priv_conf = re.compile(r'#$')
        self._prompt_more = re.compile(r'--More--')
        self._find_interfaces = re.compile(r'(((Gi)|(Te)|(Fa))+([0-9]|\/)+)')
        self._console = serial.Serial(port=port, timeout=8)
        self._command = {
            'priv': 'enable',
            'exit': 'exit',
            'interfaces': 'show interface status'
        }

if __name__ == "__main__":
    print("This is library module")