"""Module for upload basic config to switch"""

from jinja2 import Environment, FileSystemLoader
import re
from datetime import datetime
import serial.tools.list_ports as list_ports
import json

from plugins.base import BaseSwitch

class Uploader:
    """Upload config"""
    
    def print_table(self, data):
        """Print table"""
        t = ""
        count = 0
        for i in data:
            t = t + "{:12}".format(i)
            count = count + 1
            if count == 5:
                print(t)
                count = 0
                t = ""
        print(t)
    
    def _get_data_for_template(self, obj):
        """Get data for rendering template"""
        if obj['type'] == 'string':
            result = raw_input(obj['desc'] + ': ')
        elif obj['type'] == 'datetime':
            result = datetime.now().strftime("%H:%M:%S %d %B %Y")
        elif obj['type'] == 'ifaces':
            print("List of interfaces (wait)")
            ifaces = self.switch.get_interfaces()
            result = ifaces
            self.print_table(ifaces)
        elif obj['type'] == 'dict':
            result = {}
            for j in obj['content'].keys():
                result[j] = self._get_data_for_template(obj['content'][j])
        elif obj['type'] == 'table':
            result = []
            print(obj['desc'])
            while True:
                row = {}
                for j in obj['content'].keys():
                    row[j] = self._get_data_for_template(obj['content'][j])
                result.append(row)
                ask = raw_input("Do you want add vlan [y/n]: ")
                if ask == 'n':
                    break
        return result
    
    def __init__(self, templ_name):
        """Constructor"""
        # Connect to switch
        print("List of COM ports")
        for i in list_ports.comports():
            print("{:12}{}".format(i.device, i.description))
        self.port = raw_input("Enter COM port number: ")
        ftempl = open('./templates/' + templ_name + '.json', 'r')
        jtempl = json.load(ftempl)
        pack = __import__('plugins.' + jtempl['OS'])
        mod = getattr(pack, jtempl['OS'])
        swclass = getattr(mod, jtempl['OS'])
        if not issubclass(swclass, BaseSwitch):
            print("Failed plugin")
            exit()
        self.switch = swclass("COM" + self.port)
        if not self.switch.login():
            return None
        # Enter initiate data
        self.vars = {}
        for i in jtempl['vars'].keys():
            self.vars[i] = self._get_data_for_template(jtempl['vars'][i])
        # Load config template
        env = Environment(loader = FileSystemLoader("./templates"))
        self.template = env.get_template(jtempl['template'])
    
    def upload_config(self):
        """Upload config to switch"""
        rtemp = self.template.render(self.vars).splitlines()
        num_str = len(rtemp)
        count = 1
        print("Uploading config")
        for i in rtemp:
            self.switch.send_command(i.encode())
            print("{} from {}".format(count, num_str))
            count = count + 1
        print('Upload complite')
        self.switch.logout()

if __name__ == "__main__":
    print("This is library module")
