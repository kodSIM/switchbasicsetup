# Switch basic setup

This package create for automation of loading of a basic configuration on the switch.
For work with the switch it is used [pySerial](https://pypi.python.org/pypi/pyserial).

### uploader.py
	It is the main module containing a class Uploader operating loading of a configuration.

### templates

    Contains templates of configurations

### plugins

    Contains templates of plug-ins for work with switch